#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

const char* ssid = "........";
const char* password = "........";

ESP8266WebServer server(80);

void connectWifi(){
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("LEDWebServer")) {
    Serial.println("MDNS responder started");
  }
}

void indexPage() {
  server.send(200, "text/plain", "hello!");
}

void handleNotFound() {
  server.send(404, "text/plain", "Page Not Found!");
}

void configWebServer(){
  server.on("/", indexPage);

  server.on("/inline", []() {
    server.send(200, "text/plain", "this works as well");
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void setup(void) {
  Serial.begin(115200);
  connectWifi();
  configWebServer();
}

void loop(void) {
  server.handleClient();
}
