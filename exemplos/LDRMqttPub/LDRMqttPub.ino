#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define ClientID "carlosfran"
#define BROKER_MQTT "iot.eclipse.org"
#define BROKER_PORT 1883

WiFiClient wifi_client;

PubSubClient MQTT(wifi_client);

const char* ssid = ".......";
const char* password = ".......";

int ldr = A0;

void setup(void) {
  
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");
  
  pinMode(ldr, INPUT);
  
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  
  // Conectar ao broker
  MQTT.setServer(BROKER_MQTT, BROKER_PORT);
  while( !MQTT.connected() ){
    if(!MQTT.connect(ClientID) ){
      Serial.print("failed = ");
      Serial.println(MQTT.state());
      delay(3000);
    }
  }
  Serial.println("MQTT connected!");
}

int msg_count = 0;

void mqtt_pub(){
  int value = analogRead(ldr);
  String message = "{'lum': ";
  message += value;
  message += "}";

  msg_count += 1;
  
  MQTT.publish("/oficina-iot/ldr", (char*) message.c_str());
  Serial.print(msg_count);
  Serial.println(" : Message sent!");
  delay(1000);
}

void loop(void) {
  mqtt_pub();
  MQTT.loop();  
}
