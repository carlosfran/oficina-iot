#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define ClientID "carlosfran"
// #define BROKER_MQTT "iot.eclipse.org"
#define BROKER_MQTT "test.mosquitto.org"
#define BROKER_PORT 1883

WiFiClient wifi_client;

PubSubClient MQTT(wifi_client);

const char* ssid = "........";
const char* password = "........";

int ldr = A0;
const int led = D3;
int led_state = LOW;

void ledON(){
  digitalWrite(led, HIGH);
  led_state = HIGH;
}

void ledOFF(){
  digitalWrite(led, LOW);
  led_state = LOW;
}

void led_callback(char* topic, byte* payload, unsigned int length)
{
  String topic_s = String((char*) topic);
  if( topic_s.equals("/oficina-iot/led")){
    Serial.print("[");
    Serial.print(topic);
    Serial.print("] ");

    Serial.print( length );
    Serial.print("  ");

    String cmd = "";
    for(int i=0; i<length; i++){
      char c = payload[i];
      cmd += c;
    }
    
    Serial.println( cmd );
    
    if(cmd.equals("on"))
      ledON();
    else if(cmd.equals("off"))
      ledOFF();
  }else{
    Serial.println("[MQTT subscribe] Topic not identified");
  }
}

void setup(void) {
  
  Serial.begin(115200);
  pinMode(led, OUTPUT);
  pinMode(ldr, INPUT);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");
  //Wifi.connect();
  
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  reconnect();
}

void reconnect(){
  // Conectar ao broker
  MQTT.setServer(BROKER_MQTT, BROKER_PORT);
  MQTT.setCallback(led_callback);

  while(!MQTT.connected()){
    if(!MQTT.connect(ClientID)){
      Serial.print("[MQTT state] ");
      Serial.println(MQTT.state());
    }
    delay(2000);
  }
  
  if( MQTT.subscribe("/oficina-iot/led") )
    Serial.println("[MQTT subscribe] /oficina-iot/led");
  Serial.println("MQTT connected!");
}
int msg_count = 0;

void mqtt_pub(){
  int value = analogRead(ldr);
  String message = "{'lum': ";
  message += value;
  message += ", 'led':";
  message += led_state;
  message += "}";

  msg_count += 1;
  
  MQTT.publish("/oficina-iot/state", (char*) message.c_str());
  Serial.print("[ ");
  Serial.print(msg_count);
  Serial.print(" - MQTT publish]");
  Serial.println(message);
  delay(5000);
}

void loop(void) {
  if(!MQTT.connected()){
    reconnect();
  }
  mqtt_pub();
  MQTT.loop();
}
