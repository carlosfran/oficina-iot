#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

const char* ssid = "........";
const char* password = "......";

ESP8266WebServer server(80);

const int led = D3;
int led_state = LOW;

void indexPage() {
  server.send(200, "text/plain", "hello!");
}

void ledON(){
  digitalWrite(led, HIGH);
  led_state = HIGH;
  server.send(200, "text/plain", "LED on!");
}

void ledOFF(){
  digitalWrite(led, LOW);
  led_state = LOW;
  server.send(200, "text/plain", "LED off!");
}

void ledState(){
  server.send(200, "text/plain", "Status: " + String(led_state));
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void setup(void) {
  
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");
  pinMode(led, OUTPUT);
  
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("LEDWebServer")) {
    Serial.println("MDNS responder started");
  }
  
  server.on("/", indexPage);
  
  server.on("/state", ledState);

  server.on("/on", ledON);

  server.on("/off", ledOFF);

  server.on("/inline", []() {
    server.send(200, "text/plain", "this works as well");
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {
  server.handleClient();
}
