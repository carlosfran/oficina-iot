# oficina-iot

Oficina de Introdução à Internet das Coisas (Internet of Things - IoT) - slides, circuitos e códigos utilizados como exemplos.

# Ministrantes
* Carlos Fran
* Weslley Alves

# Link's Úteis:

* Slides no Google Presentations: https://docs.google.com/presentation/d/1WJACj9S5D5jxJqfbbvHVKzNbac-Ackuow8Q1mBmowps

* Documentação biblioteca ESP8266 Arduino Core: https://arduino-esp8266.readthedocs.io/en/latest/esp8266wifi/readme.html
